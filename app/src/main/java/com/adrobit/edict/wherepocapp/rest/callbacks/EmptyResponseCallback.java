/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp.rest.callbacks;

import android.util.Log;

import com.adrobit.edict.wherepocapp.events.BaseEvent;

import java.lang.reflect.InvocationTargetException;

import retrofit.client.Response;

/**
 * A generic callback for calls where the response is going to be empty. So all we do in success
 * case is generate a success event
 */
public class EmptyResponseCallback extends BaseCallback<Response> {
    private static String LOG_TAG = EmptyResponseCallback.class.getSimpleName();

    public EmptyResponseCallback(Class<? extends BaseEvent> event) {
        super(event);
    }

    @Override
    public final void success(Response response, Response response2) {
        success(response);
    }

    public void success(Response response) {
        try {
            BaseEvent event = getEvent()
                    .getDeclaredConstructor(BaseEvent.ErrorType.class)
                    .newInstance(BaseEvent.ErrorType.NO_ERROR);

            postEvent(event);
        } catch (InstantiationException e) {
            Log.e(LOG_TAG, e.getMessage());
        } catch (IllegalAccessException e) {
            Log.e(LOG_TAG, e.getMessage());
        } catch (InvocationTargetException e) {
            Log.e(LOG_TAG, e.getMessage());
        } catch (NoSuchMethodException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
}
