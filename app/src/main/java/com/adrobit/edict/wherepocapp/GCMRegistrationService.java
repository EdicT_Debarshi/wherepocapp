/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.adrobit.edict.wherepocapp.rest.RestClientV1;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

public class GCMRegistrationService extends IntentService {

    private static final String TAG = GCMRegistrationService.class.getSimpleName();

    public GCMRegistrationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            sendRegistrationToServer(token);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);

            // We retry automatically whenever the app is restarted
        }
    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        RestClientV1.getInstance().updateNotificationToken(token);

        // The event handler lives in the BaseActivity
    }
}
