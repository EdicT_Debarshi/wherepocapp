/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp.events;

import retrofit.RetrofitError;

public class GetLocationEvent extends BaseEvent {
    public GetLocationEvent(ErrorType errorType) {
        super(errorType);
    }

    public GetLocationEvent(ErrorType errorType, RetrofitError error) {
        super(errorType, error);
    }

    public GetLocationEvent(ErrorType errorType, Exception ex) {
        super(errorType, ex);
    }
}
