/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp.rest.callbacks;

import com.adrobit.edict.wherepocapp.events.BaseEvent;
import com.adrobit.edict.wherepocapp.rest.model.MeetUp;

import java.util.List;

import retrofit.client.Response;

/**
 * Callback for getting the list of clinics
 */
public class GetMeetUpData extends BaseCallback<List<MeetUp>> {

    public GetMeetUpData() {
        super(GetMeetUpsEvent.class);
    }

    @Override
    public void success(List<MeetUp> meetUps, Response response) {
        GetMeetUpsEvent event = new GetMeetUpsEvent(BaseEvent.ErrorType.NO_ERROR);

        for (MeetUp meetUp:meetUps) {
            MeetUp meet =meetUp;
            meet.save();
        }
        postEvent(event);
    }
}
