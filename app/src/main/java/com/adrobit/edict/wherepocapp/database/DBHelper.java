package com.adrobit.edict.wherepocapp.database;/*
 * Description: 
 *
 * Copyright (C) $today.year Adrobit Technologies LLP
 *
 */

import com.adrobit.edict.wherepocapp.rest.model.MeetUp;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

public class DBHelper {
    public static List<MeetUp> getAllMeetUp() {

        return SQLite
                .select()
                .from(MeetUp.class)
                .queryList();
    }
}
