package com.adrobit.edict.wherepocapp.rest.model;

import com.adrobit.edict.wherepocapp.database.PocDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by nilarnab on 7/5/16.
 */
@Table(database = PocDatabase.class)
public class MeetUp extends BaseModel {
    @PrimaryKey String id;
    @Column String meetup_id;
    @Column String member_id;
    @Column String latitude;
    @Column String longitude;
    @Column String updated_at;

    public MeetUp() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeetup_id() {
        return meetup_id;
    }

    public void setMeetup_id(String meetup_id) {
        this.meetup_id = meetup_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
