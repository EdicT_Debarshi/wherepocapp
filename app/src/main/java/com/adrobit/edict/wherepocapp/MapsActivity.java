package com.adrobit.edict.wherepocapp;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.adrobit.edict.wherepocapp.database.DBHelper;
import com.adrobit.edict.wherepocapp.events.GetMeetUpsEvent;
import com.adrobit.edict.wherepocapp.rest.RestClientV1;
import com.adrobit.edict.wherepocapp.rest.model.MeetUp;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private boolean mapReady=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Timer timer = new Timer();
        timer.schedule(new SayHelloToMyLittleFriend(), 0, 10000);
    }
    public void onEventMainThread(GetMeetUpsEvent event) {
        if (!event.isSuccess()) {
            // We will show the error dialog only if we couldn't load any clinics
            if (!Utility.handleInternetUnavailabilityError(event, this)) {
                if (!event.isHttpError()) {
                    Utility.showAlertDialog(R.string.error_unknown_title, R.string.error_unknown_details,
                            this, R.string.dialog_ok, null);
                }
            }
        }
        else{
            if(mapReady){
                List<MeetUp> allMeetUp=DBHelper.getAllMeetUp();
                mMap.clear();
                for(MeetUp meetUp:allMeetUp){
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(meetUp.getLatitude()), Double.parseDouble(meetUp.getLongitude())));
                    mMap.addMarker(markerOptions);
                }
            }
        }
        EventBus.getDefault().removeStickyEvent(GetMeetUpsEvent.class);
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapReady=true;
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble("22.564488"), Double.parseDouble("88.343587")) , 12.0f) );
        // Add a marker in Sydney and move the camera
//        LatLng myLocation = new LatLng(22.512384, 88.327378);
//        mMap.addMarker(new MarkerOptions().position(myLocation).title("Marker in Sydney"));
     //   mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15.0f));
    }
}
class SayHelloToMyLittleFriend extends TimerTask {
    public void run() {
        RestClientV1.getInstance().getMeetUps();
    }
}

