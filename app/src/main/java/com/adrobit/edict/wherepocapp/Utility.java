/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.adrobit.edict.wherepocapp.events.BaseEvent;

/**
 * Common utility functions for the entire app
 */
public class Utility {

    private static final long ALARM_TIME = 120000; //two minutes
    private static final String GOOGLE_MAPS_DIRECTIONS_BASE_URL = "google.navigation:";
    private static final String GOOGLE_MAPS_DESTINATION_QUERY = "q=";
    private static final String GOOGLE_MAPS_QUERY_MODE = "&mode=d";
    private static final String PREFIX_MAIL = "mailto:";

    /**
     * Hides keyboard from view
     *
     * @param context Context
     * @param view    View
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Shows the alert dialog with a single positive button only
     *
     * @param titleResId    Resource ID for title
     * @param messageResId  Resource ID for Message
     * @param context       Context
     * @param positiveResId Resource ID for positive Text
     * @param positive      OnClickListener for the positive button
     * @return The AlertDialog
     */
    public static AlertDialog showAlertDialog(int titleResId,
                                              int messageResId,
                                              Context context,
                                              int positiveResId,
                                              DialogInterface.OnClickListener positive) {
        String message = null;
        if (messageResId != -1) {
            message = context.getString(messageResId);
        }
        return showAlertDialog(titleResId, message, context, positiveResId, positive);
    }

    /**
     * Shows the alert dialog with a single positive button only
     *
     * @param titleResId    Resource ID for title
     * @param message       String message
     * @param context       Context
     * @param positiveResId Resource ID for positive Text
     * @param positive      OnClickListener for the positive button
     * @return The AlertDialog
     */
    public static AlertDialog showAlertDialog(int titleResId,
                                              String message,
                                              Context context,
                                              int positiveResId,
                                              DialogInterface.OnClickListener positive) {
        // Fullscreen activities use different theme compared to other activities
        // To make dialogs consistent through the app lets apply this theme.
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (message != null) {
            builder.setMessage(message);
        }

        if (titleResId != -1) {
            builder.setTitle(context.getString(titleResId));
        }

        builder.setCancelable(false);

        builder.setPositiveButton(positiveResId, positive);
        AlertDialog dialog = builder.create();
        dialog.show();

        return dialog;
    }

    /**
     * Shows the alert dialog with a positive and negative buttons
     *
     * @param titleResId    Resource ID for the title
     * @param messageResId  Resource ID for the message
     * @param context       Context
     * @param positiveResId Resource ID for positive button text
     * @param positive      OnClickHandler for positive button
     * @param negativeResId Resource ID for negative button text
     * @param negative      OnClickHandler for negative button
     * @return The alert dialog
     */
    public static AlertDialog showAlertDialog(int titleResId,
                                              int messageResId,
                                              Context context,
                                              int positiveResId,
                                              DialogInterface.OnClickListener positive,
                                              int negativeResId,
                                              DialogInterface.OnClickListener negative) {
        String message = null;
        if (messageResId != -1) {
            message = context.getString(messageResId);
        }
        return showAlertDialog(titleResId, message, context, positiveResId, positive, negativeResId, negative);
    }

    /**
     * Shows the alert dialog with a positive and negative buttons
     *
     * @param titleResId    Resource ID for the title
     * @param message       String message
     * @param context       context
     * @param positiveResId Resource ID for positive button text
     * @param positive      OnClickHandler for positive button
     * @param negativeResId Resource ID for negative button text
     * @param negative      OnClickHandler for negative button
     * @return The alert dialog
     */
    public static AlertDialog showAlertDialog(int titleResId,
                                              String message,
                                              Context context,
                                              int positiveResId,
                                              DialogInterface.OnClickListener positive,
                                              int negativeResId,
                                              DialogInterface.OnClickListener negative) {
        // Fullscreen activities use different theme compared to other activities
        // To make dialogs consistent through the app lets apply this theme.
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (message != null) {
            builder.setMessage(message);
        }

        if (titleResId != -1) {
            builder.setTitle(context.getString(titleResId));
        }

        builder.setCancelable(false);

        builder.setPositiveButton(positiveResId, positive);
        builder.setNegativeButton(negativeResId, negative);
        AlertDialog dialog = builder.create();

        dialog.show();

        return dialog;
    }

    /**
     * Shows custom snackbar alert on failure to make calls
     *
     * @param view     view to stick to
     * @param msgResId the resource id for the shown message
     */
//    public static void showSnackBar(View view, int msgResId) {
//        final Snackbar snackbar = Snackbar.make(view,
//                msgResId,
//                Snackbar.LENGTH_INDEFINITE);
//
//        snackbar.setAction(R.string.close_snack_bar, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                snackbar.dismiss();
//            }
//        });
//        snackbar.show();
//    }

    /**
     * Calls the system service to check if we have Internet access
     *
     * @return True if internet is connected, false otherwise
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Are we running the app in debug mode
     *
     * @return True if in debug mode, false otherwise
     */
    public static boolean isDebug() {
        return (0 != (App.getAppContext().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    /**
     * Gets the pref from Shared Preferences
     *
     * @param stringResId  Key's string resource ID
     * @param defaultValue default value to be returned if the key is not found
     * @return Value from SharedPref
     */
    public static String getStringPreference(int stringResId, String defaultValue) {
        Context context = App.getAppContext();
        SharedPreferences prefs = App.getSharedPreferences();
        return prefs.getString(context.getString(stringResId), defaultValue);
    }

    /**
     * Gets the pref from Shared Preferences
     *
     * @param stringResId  Key's string resource ID
     * @param defaultValue default value to be returned if the key is not found
     * @return Value from SharedPref
     */
    public static boolean getBooleanPreference(int stringResId, boolean defaultValue) {
        Context context = App.getAppContext();
        SharedPreferences prefs = App.getSharedPreferences();
        return prefs.getBoolean(context.getString(stringResId), defaultValue);
    }

    /**
     * Sets the key and String value in Shared Preferences
     *
     * @param stringResId Key's string resource ID
     * @param value       Value to be stored
     */
    public static void storeStringPreference(int stringResId, String value) {
        Context context = App.getAppContext();
        SharedPreferences prefs = App.getSharedPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(context.getString(stringResId), value);
        editor.apply();
    }

    /**
     * Sets the key and boolean value in Shared Preferences
     *
     * @param stringResId Key's string resource ID
     * @param value       Value to be stored
     */
    public static void storeBooleanPreference(int stringResId, boolean value) {
        Context context = App.getAppContext();
        SharedPreferences prefs = App.getSharedPreferences();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(context.getString(stringResId), value);
        editor.apply();
    }

    /**
     * Gets the tinted drawable in backwards compatible way
     *
     * @param drawableResource the drawable
     * @param tintColor        Color ID for the tint
     * @return Tinted drawable
     */
    public static Drawable getTintedDrawableCompat(Drawable drawableResource, int tintColor) {
        Resources r = App.getAppContext().getResources();
        Drawable drawable = DrawableCompat.wrap(drawableResource);
        DrawableCompat.setTint(drawable, r.getColor(tintColor));
        return drawable;
    }

    /**
     * Validates the passed in phone number for Singapore without the country code
     *
     * @param phone Phone number to validated
     * @return Valid or not
     */
    public static boolean isPhoneValid(String phone) {
        return phone != null && phone.length() == 10 && TextUtils.isDigitsOnly(phone);
    }

    /**
     * Returns whether the passed email is valid
     *
     * @param email Email to be validated
     * @return Valid or not
     */
    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Sets alarm for calling alarmReceiver when it expires
     */
//    public static void setAlarm() {
//        PendingIntent alarmIntent;
//        AlarmManager alarmManager = (AlarmManager) App.getAppContext().getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(App.getAppContext(), AlarmReceiver.class);
//        alarmIntent = PendingIntent.getBroadcast(App.getAppContext(), 0, intent, 0);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                    SystemClock.elapsedRealtime() + ALARM_TIME,
//                    alarmIntent);
//        } else {
//            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                    SystemClock.elapsedRealtime() + ALARM_TIME,
//                    alarmIntent);
//        }
//    }

    /**
     * Starts a broadcast receiver at runtime to read sms broadcast and parse our backened generated OTP
     *
     * @param context
     */
//    public static void startSmsParsingReceiver(Context context) {
//        IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(900);
//        context.registerReceiver(new ReadSmsBroadcast(), filter);
//    }

    /**
     * Checks whether notifications to show ticket updates enabled
     *
     * @return true if notifications are enabled
     */
//    public static boolean isTicketNotificationsEnabled() {
//        Context context = App.getAppContext();
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//        return prefs.getBoolean(context.getString(R.string.notifications_ticket_key),
//                Boolean.parseBoolean(context.getString(R.string.notifications_ticket_default)));
//    }

    /**
     * Used to get a share intent with the passed-in string
     *
     * @param shareText Text to be shared
     * @return An intent to share the share string.
     * <p/>
     */
    public static Intent buildShareIntent(String shareText) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        return sendIntent;
    }

    /**
     * Opens the email intent
     *
     * @param context Context
     * @param to      To address
     */
//    public static void sendEmail(Context context, String to) {
//        Intent emailIntent = new Intent();
//        emailIntent.setAction(Intent.ACTION_SENDTO);
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
//        Uri uri = Uri.parse(PREFIX_MAIL);
//        emailIntent.setData(uri);
//        context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.chooser_email_client)));
//    }

    /**
     * It generates an intent which is used to open maps activity
     * and get driving navigation from users's current location to clinic....
     *
     * @param fullAddress the full address string found as property in a clinic object
     * @return an intent to open maps activity and start navigation
     */
    public static Intent getDirections(String fullAddress) {
        String mapsIntentUrl = GOOGLE_MAPS_DIRECTIONS_BASE_URL +
                GOOGLE_MAPS_DESTINATION_QUERY + fullAddress +
                GOOGLE_MAPS_QUERY_MODE;
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(mapsIntentUrl));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * Generates dial-able phone number with prefixed country code
     *
     * @param phoneNumber A nine digit phone number, including an hyphen in between.
     *                    Example : 1234-5673
     * @return A totally legal phone number with country code as prefix.
     * Example : 6512345673
     */
    public static String getPrefixedPhoneNumber(String phoneNumber) {
        //if phoneNumber = 1234-1234
        if (phoneNumber.length() == 9) {
            StringBuilder builder = new StringBuilder(Constants.SGP_PHONE_PREFIX);
            // builder = 65
            if (phoneNumber.charAt(4) == '-') {
                builder.append(phoneNumber.substring(0, 4));  // builder = 651234
                builder.append(phoneNumber.substring(5, 9));  // builder = 6512341234
            } else {
                //this part is faced when there is no dash and erroneous phone number comes
                //Example 12341234 and a 'space'
                builder.append(phoneNumber.trim()); //builder = 6512341234
            }
            return builder.toString();
        }
        return null;
    }

    /**
     * Checks whether we have SMS read permission
     *
     * @param context the current activity context
     * @return whether permission(run time) was granted or not
     */
    public static boolean checkForReadSMSPermission(Context context) {
        return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                context.checkSelfPermission(android.Manifest.permission.READ_SMS)
                        != PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Cutoff height is a constant height that the constant elements of every intro slide
     * screens are going to have
     * 60dp top margin + height of title + 100dp card view + 80 dp footer margin;
     *
     * @param metrics display metrics according to device and activity
     * @return Height after subtracting the cutoff height.
     */
    public static int getHeightForIntroImages(DisplayMetrics metrics) {
        float heightCutoff = (60 + 40 + 100 + 80) * metrics.density;
        float heightOfImage = (metrics.heightPixels - heightCutoff);
        return (int) heightOfImage;
    }

    public static boolean handleInternetUnavailabilityError(BaseEvent event, Context context) {
        if (event.isInternetUnavailable()) {
            Utility.showAlertDialog(R.string.network_error_title,
                    R.string.network_error_details,
                    context, R.string.dialog_ok, null);

            return true;
        }
        return false;
    }
}