/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp.rest.service;

import com.adrobit.edict.wherepocapp.rest.callbacks.EmptyResponseCallback;
import com.adrobit.edict.wherepocapp.rest.model.MeetUp;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * The Service interface for all backend calls - Currently V1
 */
public interface ApiServiceV1 {

    @GET("/meetups/{id}")
    void getMeetUpLoc(@Path("id") String id,
                      Callback<List<MeetUp>> meetUpCallback);
    @FormUrlEncoded
    @POST("/meetups/1/update_location")
    void setUpdatedLocation(@Field("member_id")String memberId,
                            @Field("latitude") double latitude,
                            @Field("longitude") double longitude,
                            EmptyResponseCallback cb);

    @FormUrlEncoded
    @POST("/users/{user_id}/device_token")
    void updateNotificationToken(@Path("user_id") String userId,
                                 @Field("device_token") String deviceToken,
                                 EmptyResponseCallback cb);

}
