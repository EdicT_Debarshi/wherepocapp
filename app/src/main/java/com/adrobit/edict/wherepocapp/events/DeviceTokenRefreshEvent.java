/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp.events;

import retrofit.RetrofitError;

/**
 * Called when the retrofit call to refresh device token on backend finishes
 */
public class DeviceTokenRefreshEvent extends BaseEvent {

    public DeviceTokenRefreshEvent(ErrorType errorType) {
        this(errorType, null);
    }

    public DeviceTokenRefreshEvent(ErrorType errorType, RetrofitError error) {
        super(errorType, error);
    }

    @Override
    public boolean getPostSticky() {
        return true;
    }
}
