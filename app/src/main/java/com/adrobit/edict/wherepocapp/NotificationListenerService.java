/*
 * Copyright (c) 2016 Adrobit Technologies LLP.
 */

package com.adrobit.edict.wherepocapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;


public class NotificationListenerService extends GcmListenerService {

    private static final String TAG = NotificationListenerService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
       // if (!Utility.isTicketNotificationsEnabled()) {
//            return;
//        }
        Log.d("notif",data.toString());
//
//        int notificationType = Integer.parseInt(data.getString("notification_type", "0"));
//        NotificationType messageType = NotificationType.values()[notificationType];
//
//        Log.d(TAG, "From: " + from);
//        Log.d(TAG, "NotificationType: " + notificationType);
//
//        if (messageType == NotificationType.GENERAL) {
//            // These are for general messages
//            String title = data.getString("title");
//            String message = data.getString("message");
//            Log.d(TAG, "Title: " + title);
//            Log.d(TAG, "Message: " + message);
//
//            sendNotification(title, message, messageType, null);
//        } else if (messageType == NotificationType.TICKETS_BOOKED) {
//            sendNotification(getString(R.string.app_name),
//                    getString(R.string.notification_message_tickets_booked), messageType, null);
//        } else {
//            String ticketId = data.getString("ticket_id");
//            String waitBefore = data.getString("wait_before");
//            String checkedInStr = data.getString("checked_in");
//            String checkedInPatientsBeforeThis = data.getString("checkedInPatientsBeforeThis");
//            boolean checkedIn = Boolean.parseBoolean(checkedInStr);
//
//            Log.d(TAG, "ticketId: " + ticketId);
//            Log.d(TAG, "WaitBefore: " + waitBefore);
//            Log.d(TAG, "checkedIn: " + checkedInStr);
//            Log.d(TAG, "checkedIn patients: " + checkedInPatientsBeforeThis);
//
//            // TODO: Should we update all tickets in the group? Not needed unless we start
//            // using the other tickets to read information
//            Ticket t = TicketDbHelper.getTicketById(ticketId);
//            if (t != null) {
//                Ticket.TicketStatus status = t.getStatus();
//                if (status == Ticket.TicketStatus.waiting
//                        || status == Ticket.TicketStatus.pending_checkin
//                        || status == Ticket.TicketStatus.visiting) {
//                    switch (messageType) {
//                        case REMINDER:
//                            if (waitBefore != null && checkedInPatientsBeforeThis != null &&
//                                    Integer.parseInt(t.getWaitingBeforeCurrent()) >= Integer.parseInt(waitBefore)) {
//                                t.setWaitingBeforeCurrent(waitBefore);
//                                t.setCheckedInPatientsBeforeThis(Integer.parseInt(checkedInPatientsBeforeThis));
//                            } else {
//                                // Ignore as this is likely a stale notification
//                                return;
//                            }
//                            break;
//                        case YOUR_TURN:
//                            t.setStatus(Ticket.TicketStatus.visiting);
//                            break;
//                        case TURN_MISSED:
//                            t.setStatus(Ticket.TicketStatus.no_show);
//                            break;
//                        case TICKETS_CANCELLED:
//                            t.setStatus(Ticket.TicketStatus.cancelled);
//                            break;
//                        case TICKET_STATUS_VISITED:
//                            t.setStatus(Ticket.TicketStatus.visited);
//                            break;
//                    }
//                    t.save();
//                }
//                // This is not needed anymore as home activity just has static information
//                // This should be added back for the following scenario when implemented:
//                //
//                // Someone books a ticket through the web interface for the current user. The
//                // backend needs to send a notification to the app and the app needs to refresh
//                // the home screen if open
//                //
//                //EventBus.getDefault().post(new RefreshTicketsEvent(BaseEvent.ErrorType.NO_ERROR));
//
//                if (messageType != NotificationType.TICKET_STATUS_VISITED
//                        && !t.shouldHideNotifications()) {
//                    String message = buildMessage(messageType, waitBefore, checkedIn);
//                    sendNotification(t.getClinicName(), message, messageType, t.getId());
//                }
//            }
//        }
    }

    private String buildMessage(NotificationType type, String waitBefore, boolean checkedIn) {
//        if (type == NotificationType.YOUR_TURN) {
//            return getString(R.string.notification_your_turn);
//        } else if (type == NotificationType.REMINDER) {
//            StringBuilder builder = new StringBuilder();
//            if (waitBefore.equals("0")) {
//                builder.append(getString(R.string.notification_reminder_next));
//                if (!checkedIn) {
//                    builder.append(getString(R.string.notification_reminder_asap_message));
//                }
//            } else {
//                builder.append(getString(R.string.notification_reminder_prefix))
//                        .append(waitBefore)
//                        .append(getString(R.string.notification_reminder_suffix));
//
//                if (!checkedIn) {
//                    builder.append(getString(R.string.notification_reminder_checkin));
//                }
//            }
//            return builder.toString();
//        } else if (type == NotificationType.TURN_MISSED) {
//            return getString(R.string.notification_missed_turn);
//        } else if (type == NotificationType.TICKETS_CANCELLED) {
//            return getString(R.string.notification_message_tickets_cancelled);
//        } else {
//            return null;
//        }
        return null;
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String title, String message, NotificationType notificationType, String ticketId) {
//        Intent intent;
//        if (notificationType == NotificationType.TICKETS_BOOKED) {
//            intent = new Intent(this, SplashActivity.class);
//        } else {
//            intent = new Intent(this, HomeActivity.class);
//            intent.setAction(HomeActivity.ACTION_SHOW_TICKET);
//            intent.putExtra(HomeActivity.TICKET_ID, ticketId);
//        }
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_healing_white_png)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setColor(getResources().getColor(R.color.colorPrimary))
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setContentIntent(pendingIntent)
//                .setPriority(NotificationCompat.PRIORITY_HIGH);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            notificationBuilder.setCategory(Notification.CATEGORY_STATUS).setVisibility(Notification.VISIBILITY_PUBLIC);
//        }
//
//        if (notificationType == NotificationType.REMINDER) {
//            Intent cancelIntent = new Intent(this, HomeActivity.class);
//            cancelIntent.setAction(HomeActivity.ACTION_CANCEL_TICKET);
//            cancelIntent.putExtra(HomeActivity.TICKET_ID, ticketId);
//            cancelIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            PendingIntent cancelTicketIntent = PendingIntent.getActivity(this,
//                    0, cancelIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);
//
//            Intent remindIntent = new Intent(this, HomeActivity.class);
//            remindIntent.setAction(HomeActivity.ACTION_NO_REMINDER);
//            remindIntent.putExtra(HomeActivity.TICKET_ID, ticketId);
//            remindIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            PendingIntent dontRemindIntent = PendingIntent.getActivity(this,
//                    0, remindIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);
//
//            notificationBuilder
//                    .addAction(new NotificationCompat.Action(R.drawable.ic_delete_white_png, getString(R.string.notification_action_cancel_ticket), cancelTicketIntent))
//                    .addAction(new NotificationCompat.Action(R.drawable.ic_notifications_off_white_png, getString(R.string.notification_action_dont_remind), dontRemindIntent));
//        } else if (notificationType == NotificationType.YOUR_TURN) {
//            // We want both vibration and alarm type notification in this case
//            soundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bell);
//            notificationBuilder.setVibrate(new long[]{0, 500, 200, 500, 200, 500});
//        } else if (notificationType == NotificationType.TURN_MISSED
//                || notificationType == NotificationType.TICKETS_CANCELLED) {
//            //Now we provide option of re-booking the ticket and delete the current ticket
//            Intent reBookIntent = new Intent(this, HomeActivity.class);
//            reBookIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            reBookIntent.setAction(HomeActivity.ACTION_TURN_MISSED);
//            reBookIntent.putExtra(HomeActivity.TICKET_ID, ticketId);
//            PendingIntent reBookTicketIntent = PendingIntent.getActivity(this,
//                    0, reBookIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);
//            notificationBuilder
//                    .addAction(new NotificationCompat.Action(R.drawable.ic_autorenew_white_png, "Re-book", reBookTicketIntent));
//
//        } else {
//            // For notifications which are not reminders, we want then to auto-cancel
//            notificationBuilder.setAutoCancel(true);
//        }
//
//        notificationBuilder.setSound(soundUri);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public enum NotificationType {
        GENERAL,
        REMINDER,
        YOUR_TURN,
        TURN_MISSED,
        TICKETS_BOOKED,
        TICKETS_CANCELLED,
        TICKET_STATUS_VISITED
    }
}
