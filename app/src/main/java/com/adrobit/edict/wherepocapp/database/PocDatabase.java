package com.adrobit.edict.wherepocapp.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by nilarnab on 7/5/16.
 */
@Database(name = PocDatabase.NAME, version = PocDatabase.VERSION)
public class PocDatabase {
    public static final String NAME = "pocdb";

    public static final int VERSION = 2;
}
